var email = localStorage.getItem("Email")

alert(email)

window.onload = function() {
    fetch("/getUser/"+email)
    .then(response => response.text())
    .then(data => showData(data))
}

function showData(data){
    var userdata = JSON.parse(data)
    // console.log(userdata)

    document.querySelector("#proimg").src = userdata.Imags
    document.querySelector(".nn").innerHTML = userdata.fname + " " + userdata.lname
    document.querySelector(".fn").value = userdata.fname
    document.querySelector(".ln").value = userdata.lname
    document.querySelector(".e").value = userdata.email
}

function Update(){
    document.querySelector("#picture").addEventListener("change", function(){
   
        const dataurl = new FileReader();
        dataurl.addEventListener("load", ()=>{
            localStorage.setItem("image-dataurl", dataurl.result)
        })
        dataurl.readAsDataURL(this.files[0])
    })
    let ims = localStorage.getItem("image-dataurl")
    alert(ims)

    
    let newData = {
        Imags: localStorage.getItem("image-dataurl"),
        fname: document.querySelector(".fn").value,
        lname: document.querySelector(".ln").value,
        email: document.querySelector(".e").value
    }
    fetch("/userupdate/"+email, {
        method: "PUT",
        body: JSON.stringify(newData),
        headers:{"Content-type":"application/json; charset=UTF-8"}
    })
    .then(respose => respose.text())
    .then(data => showNewData(data))
    alert("Update successfull")

}
function showNewData(data){
    let updatedData = JSON.parse(data)
    localStorage.setItem("Email", updatedData.email)

    console.log(updatedData)
    document.querySelector("#proimg").src = updatedData.Imags
    document.querySelector(".nn").innerHTML = updatedData.fname + " " + updatedData.lname
}

