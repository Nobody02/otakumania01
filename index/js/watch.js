// JavaScript code here...

let editAnimeId = null; // Global variable to store the ID of the anime being edited

let email = localStorage.getItem("Email")

// alert(email)

window.onload = function() {
  // alert('OIIII')
  getAllAnime()

}
function getAllAnime() {
  axios.get("/api/anime/"+email)
    .then(function(response) {
      const animeList = response.data;
      const tableBody = document.querySelector("#anime-list tbody");

      // Clear table body
      tableBody.innerHTML = "";

      // Loop through animeList and create table rows
      animeList.forEach(function(anime) {
        const row = document.createElement("tr");

        // Create table cells for ID, title, season, and actions
        const idCell = document.createElement("td");
        idCell.textContent = anime.id;
        row.appendChild(idCell);

        const titleCell = document.createElement("td");
        titleCell.textContent = anime.title;
        row.appendChild(titleCell);

        const seasonCell = document.createElement("td");
        seasonCell.textContent = anime.season;
        row.appendChild(seasonCell);

        const actionsCell = document.createElement("td");

        // Create edit button
        const editButton = document.createElement("button");
        editButton.textContent = "Edit";
        editButton.style.color = "green";
        editButton.style.marginRight = "5px";
        editButton.addEventListener("click", function() {
          openEditPopup(anime);
        });
        actionsCell.appendChild(editButton);

        // Create delete button
        const deleteButton = document.createElement("button");
        deleteButton.textContent = "Delete";
        deleteButton.style.color = "red";
        deleteButton.addEventListener("click", function() {
          deleteAnime(anime.id);
        });
        actionsCell.appendChild(deleteButton);

        row.appendChild(actionsCell);

        // Append row to the table body
        tableBody.appendChild(row);
      });
    })
    .catch(function(error) {
      console.log(error);
    });
}

// Function to open the edit popup with pre-filled data
function openEditPopup(anime) {
  const popupBox = document.querySelector(".popup-box");
  const titleInput = document.querySelector("#anime-title");
  const seasonInput = document.querySelector("#anime-season");
  const addButton = document.querySelector("#add-anime-btn");

  titleInput.value = anime.title;
  seasonInput.value = anime.season;
  addButton.textContent = "Update Anime";

  editAnimeId = anime.id;

  popupBox.style.display = "flex";
}

// Function to create a new anime
function addAnime() {
  const titleInput = document.querySelector("#anime-title");
  const seasonInput = document.querySelector("#anime-season");

  const animeData = {
    title: titleInput.value,
    season: seasonInput.value,
    email: email
  };

  axios.post("/api/anime", animeData)
    .then(function(response) {
      console.log(response.data.message);
      titleInput.value = "";
      seasonInput.value = "";
      const popupBox = document.querySelector(".popup-box");
      popupBox.style.display = "none";
      popupBox.style.padding = "10px";
      getAllAnime(); // Refresh the table after creating a new anime
    })
    .catch(function(error) {
      console.log(error);
    });
}

// Function to update an anime
function updateAnime() {
  const titleInput = document.querySelector("#anime-title");
  const seasonInput = document.querySelector("#anime-season");

  const animeData = {
    id: editAnimeId,
    title: titleInput.value,
    season: seasonInput.value,
    email: email

  };

  axios.put("/api/anime", animeData)
    .then(function(response) {
      console.log(response.data.message);
      titleInput.value = "";
      seasonInput.value = "";
      const popupBox = document.querySelector(".popup-box");
      popupBox.style.display = "none";
      popupBox.style.padding = "10px"
      editAnimeId =
      null;
      getAllAnime(); // Refresh the table after updating the anime
    })
    .catch(function(error) {
    console.log(error);
  });
}

// Function to delete an anime
function deleteAnime(id) {
  axios.delete(`/api/anime/${id}`)
  .then(function(response) {
    console.log(response.data.message);
    getAllAnime(); // Refresh the table after deleting an anime
  })
  .catch(function(error) {
  console.log(error);
  });
}

// Code for opening the Add Anime popup
const addBoxButton = document.querySelector(".add-box");
addBoxButton.addEventListener("click", function() {
  const popupBox = document.querySelector(".popup-box");
  const titleInput = document.querySelector("#anime-title");
  const seasonInput = document.querySelector("#anime-season");
  const addButton = document.querySelector("#add-anime-btn");

  titleInput.value = "";
  seasonInput.value = "";
  addButton.textContent = "Add Anime";
  popupBox.style.display = "flex";
});

// Event listener for the Close button in the popup box
const closeIcon = document.querySelector(".close-icon");
closeIcon.addEventListener("click", function() {
  const popupBox = document.querySelector(".popup-box");
  popupBox.style.display = "none";
  editAnimeId = null;
});

// Event listener for the Add Anime button in the popup box
const addAnimeForm = document.querySelector("#add-anime-form");
addAnimeForm.addEventListener("submit", function(event) {
  event.preventDefault();
  if (editAnimeId) {
    updateAnime();
  } else {
    addAnime();
  }
});

// Call getAllAnime function to display initial anime data
getAllAnime();

