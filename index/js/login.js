function login(){
    var data = {
        email : document.getElementById("email").value,
        password:document.getElementById("pw").value
    }
    fetch("/login",{
        method:"POST",
        body:JSON.stringify(data),
        headers:{"content-type":"application/json"}
    }).then(response =>{
        if (response.status==200){
            localStorage.setItem("Email", data.email)
            window.location.href = "/index/home.html"
        }else if (response.status==401){
            alert("Enter your correct credentials")
        }
    })
    if (data.email == ""){
        alert("Enter your Credentials");
    }else if (data.password ==""){
        alert("Enter your Credentials")
    }
}

function Logout(){
    fetch("/logout")
    .then(res => {
        if (res.ok){
            window.open("index.html","_self")
        }else{
            throw new Error(res.statusText)
        }
    }).catch(e =>{
        alert(e)
    })
}