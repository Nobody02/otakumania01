const passwordE1 = document.querySelector("#password");
const confirmPasswordE1 = document.querySelector("#confirm-password");
const form = document.querySelector("#signup");

form.addEventListener("submit",function(e) {
    e.preventDefault();

    let isPasswordValid = checkPassword(),
        isConfirmPasswordValid = checkConfirmPassword();
    let isFormValid = isPasswordValid && isPasswordValid && isConfirmPasswordValid;
    if(isFormValid){
        window.open('http://127.0.0.1:5500/login/login.html')
        window.close('http://127.0.0.1:5500/Change%20password/changePassword.html')
    }
})

let isRequired = value => value=== ''?false:true;

const isBetween = (length, min, max) => length < min || length >max? false: true;

let showError = (input, message) => {
    let formField = input.parentElement;
    formField.classList.remove("success");
    formField.classList.add("error");

    const error = formField.querySelector("small");
    error.textContent = message;
}

const showSuccess = (input) => {
    const formField = input.parentElement;
    formField.classList.remove("error");
    formField.classList.add("success");

    formField.querySelector("small").textContent = "";
}


function checkConfirmPassword(){
    let valid = false;
    const passwordCheck = confirmPasswordE1.value.trim();
    if(!isRequired(passwordCheck)){
        showError(confirmPasswordE1,"You will have to confirm to proceed");
    }
    else if(!passwordMatch()){
        showError(confirmPasswordE1,"Password is not matching");
    }
    else {
        showSuccess(confirmPasswordE1);
        valid = true;
    }
    return valid;
} 
const passwordMatch = () => {
    if(passwordE1.value == confirmPasswordE1.value){
        return true;
    }
}

var email = localStorage.getItem("Email")

window.onload = function() {
    fetch("/getUser/"+email)
    .then(response => response.text())
    .then(data => showData(data))
}

function showData(data){
    var userdata = JSON.parse(data)
    // console.log(userdata)
    // document.querySelector(".e").value = userdata.email
    document.querySelector(".p1").value = userdata.password
}

function Update(){
        let newData = {
        email: email,
        password: document.querySelector(".p2").value
    }
    fetch("/passupdate/"+email, {
        method: "PUT",
        body: JSON.stringify(newData),
        headers:{"Content-type":"application/json; charset=UTF-8"}
    })
    .then(respose => respose.text())
    .then(data => showNewData(data))
    alert("Password Updated")

}
function showNewData(data){
    let updatedData = JSON.parse(data)
    localStorage.setItem("Email", updatedData.email)

    console.log(updatedData)
    document.querySelector(".p1").value = updatedData.password
    document.querySelector(".p1").value = document.querySelector(".p2").value
}
