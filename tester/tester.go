package tester

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

func main() {
	dbInfo := "postgres://postgres:postgres@localhost/my_db?sslmode=require"

	var err error
	db, err = sql.Open("postgres", dbInfo)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	router := mux.NewRouter()
	router.HandleFunc("/api/anime", getAllAnime).Methods("GET")
	router.HandleFunc("/api/anime", createAnime).Methods("POST")
	router.HandleFunc("/api/anime", updateAnime).Methods("PUT")
	router.HandleFunc("/api/anime/{id}", deleteAnime).Methods("DELETE")
	router.PathPrefix("/index/").Handler(http.StripPrefix("/index/", http.FileServer(http.Dir("index"))))

	// Signup and Signin
	router.HandleFunc("/signup", AddUserHandler).Methods("POST")
	router.HandleFunc("/login", LoginHandler).Methods("POST")
	router.HandleFunc("/logout", LogoutHandler).Methods("GET")

	router.HandleFunc("/userupdate/{email}", UpdateUserHadler).Methods("PUT")
	router.HandleFunc("/passupdate/{email}", UpdatePassHandler).Methods("PUT")
	router.HandleFunc("/getUser/{email}", GeteUserHadler).Methods("GET")

	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		tmpl, err := template.ParseFiles("index/landing.html")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		err = tmpl.Execute(w, nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	})

	log.Println("Server started on port 9999")
	log.Fatal(http.ListenAndServe(":9999", router))
}

// **********************************************************db*********************************************************************

const (
	postgres_host     = "db"
	postgres_port     = 5432
	postgres_user     = "postgres"
	postgres_password = "postgres"
	postgres_dbname   = "my_db"
)

func init() {
	//creating the connection string
	db_info := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=require", postgres_host, postgres_port, postgres_user, postgres_password, postgres_dbname)

	var err error
	//open connection to database

	Db, err = sql.Open("postgres", db_info)

	if err != nil {
		panic(err)
	} else {
		log.Println("Database successfully configured")
	}
}

// **********************************************************Model*********************************************************************
type Anime struct {
	ID     int    `json:"id"`
	Title  string `json:"title"`
	Season string `json:"season"`
}

func (a *Admin) GetAdminDetail(email string) error {
	const queryGetCourse = "SELECT * FROM admin WHERE Email = $1;"
	return Db.QueryRow(queryGetCourse, email).Scan(&a.Email, &a.FirstName, &a.LastName, &a.Password, &a.Imags)
}
func (a *Admin) UpdateAdminDetails(email string) error {
	const queryUpdateProfileDetails = "UPDATE admin set FirstName=$1, LastName=$2, Email = $3, Imags =$4 WHERE Email=$5 RETURNING Email"
	err := Db.QueryRow(queryUpdateProfileDetails, a.FirstName, a.LastName, a.Email, a.Imags, email).Scan(&a.Email)
	return err
}
func (a *Admin) UpdatePassDetails(email string) error {
	const queryUpdatePassDetails = "UPDATE admin set Password = $1, Email = $2 WHERE Email=$3 RETURNING Email"
	err := Db.QueryRow(queryUpdatePassDetails, a.Password, a.Email, email).Scan(&a.Email)
	return err
}

type Admin struct {
	FirstName string `json:"fname"`
	LastName  string `json:"lname"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	Imags     string
}

func (a *Admin) Create() error {
	const queryCreateUser = "INSERT INTO admin (FirstName,LastName,Email, Password, Imags) VALUES($1,$2,$3,$4,$5)"
	_, err := Db.Exec(queryCreateUser, a.FirstName, a.LastName, a.Email, a.Password, a.Imags)
	return err
}

func (a *Admin) Check(email string) error {
	const queryCheck = "Select * from admin where Email = $1;"
	err := Db.QueryRow(queryCheck, email).Scan(&a.FirstName, &a.LastName, &a.Email, &a.Password, &a.Imags)
	// fmt.Println(err.Error())
	return err
}

// ********************************************************Controller*************************************************************
var db *sql.DB

func getAllAnime(w http.ResponseWriter, r *http.Request) {
	rows, err := db.Query("SELECT * FROM anime")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	animeList := []Anime{}
	for rows.Next() {
		var anime Anime
		err := rows.Scan(&anime.ID, &anime.Title, &anime.Season)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		animeList = append(animeList, anime)
	}

	json.NewEncoder(w).Encode(animeList)
}

func createAnime(w http.ResponseWriter, r *http.Request) {
	var anime Anime
	err := json.NewDecoder(r.Body).Decode(&anime)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	_, err = db.Exec("INSERT INTO anime (title, season) VALUES ($1, $2)", anime.Title, anime.Season)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	response := struct {
		Message string `json:"message"`
	}{
		Message: "Anime created successfully",
	}

	json.NewEncoder(w).Encode(response)
}

func updateAnime(w http.ResponseWriter, r *http.Request) {
	var anime Anime
	err := json.NewDecoder(r.Body).Decode(&anime)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	_, err = db.Exec("UPDATE anime SET title = $1, season = $2 WHERE id = $3", anime.Title, anime.Season, anime.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	response := struct {
		Message string `json:"message"`
	}{
		Message: "Anime updated successfully",
	}

	json.NewEncoder(w).Encode(response)
}

func deleteAnime(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]
	if id == "" {
		http.Error(w, "Missing anime ID parameter", http.StatusBadRequest)
		return
	}

	_, err := db.Exec("DELETE FROM anime WHERE id = $1", id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	response := struct {
		Message string `json:"message"`
	}{
		Message: "Anime deleted successfully",
	}

	json.NewEncoder(w).Encode(response)
}

var Db *sql.DB

func UpdatePassHandler(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]

	var ad Admin
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&ad)
	if err != nil {
		ResponseWithError(w, http.StatusBadRequest, "invilid json body")
		return
	}

	updatePassErr := ad.UpdatePassDetails(email)
	if updatePassErr != nil {
		switch updatePassErr {
		case sql.ErrNoRows:
			ResponseWithError(w, http.StatusNotFound, "user not found")
		default:
			ResponseWithError(w, http.StatusInternalServerError, updatePassErr.Error())
		}
	} else {
		ResponseWithJson(w, http.StatusOK, ad)
	}
}

func GeteUserHadler(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]

	var ad Admin
	getErr := ad.GetAdminDetail(email)
	if getErr != nil {
		fmt.Print("couldn't get the data from the database")
		ResponseWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	ResponseWithJson(w, http.StatusOK, ad)
}

func UpdateUserHadler(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	var ad Admin
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&ad)
	if err != nil {
		ResponseWithError(w, http.StatusBadRequest, "invilid json body")
		return
	}

	updateTitleErr := ad.UpdateAdminDetails(email)
	if updateTitleErr != nil {
		switch updateTitleErr {
		case sql.ErrNoRows:
			ResponseWithError(w, http.StatusNotFound, "user not found")
		default:
			ResponseWithError(w, http.StatusInternalServerError, updateTitleErr.Error())
		}
	} else {
		ResponseWithJson(w, http.StatusOK, ad)
	}
}

func ResponseWithError(w http.ResponseWriter, code int, message string) {
	ResponseWithJson(w, code, map[string]string{"error": message})
}

func ResponseWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)

}

func AddUserHandler(w http.ResponseWriter, r *http.Request) {
	var admin Admin
	if err := json.NewDecoder(r.Body).Decode(&admin); err != nil {
		ResponseWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()

	saveErr := admin.Create()
	if saveErr != nil {
		ResponseWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	fmt.Println(admin)
	ResponseWithJson(w, http.StatusCreated, map[string]string{"status": "admin added"})
}

var admin Admin

func LoginHandler(w http.ResponseWriter, r *http.Request) {

	if err := json.NewDecoder(r.Body).Decode(&admin); err != nil {
		ResponseWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	email := admin.Email
	var admin2 Admin
	loginErr := admin2.Check(email)

	if loginErr != nil {
		switch loginErr {
		case sql.ErrNoRows:
			ResponseWithError(w, http.StatusUnauthorized, "invalid login")
		default:
			ResponseWithError(w, http.StatusBadRequest, "error in database")
		}
		return
	}
	fmt.Println(admin.Password, "requst")
	fmt.Println(admin2.Password, "database")
	if admin.Password != admin2.Password {
		ResponseWithError(w, http.StatusUnauthorized, "invalid login")
		return
	}

	cookie := http.Cookie{
		Name: "admin-cookie",
		// Value: email +admin.Password,
		Value:   "meow",
		Expires: time.Now().Add(30 * time.Minute),
		Secure:  true,
	}
	//set cookie and send back to client
	http.SetCookie(w, &cookie)
	ResponseWithJson(w, http.StatusOK, map[string]string{"message": "login success"})
}

func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:    "admin-cookie",
		Expires: time.Now(),
	})
	fmt.Println("logout successful")
	ResponseWithJson(w, http.StatusOK, map[string]string{"message": "logout successful"})
}

func VerifyCookie(w http.ResponseWriter, r *http.Request) bool {
	cookie, err := r.Cookie("admin-cookie")
	if err != nil {
		switch err {
		case http.ErrNoCookie:
			ResponseWithError(w, http.StatusSeeOther, "cookie not set")
		default:
			ResponseWithError(w, http.StatusInternalServerError, "internal server error")
		}
		return false
	}
	fmt.Println(admin.Email, "email")
	fmt.Println(admin.Password, "password")
	if cookie.Value != "meow" {
		ResponseWithError(w, http.StatusSeeOther, "invalid cookie")
		return false
	}
	return true
}

// ******************************************Testing********************************************
