DROP TABLE anime;

create table anime (
    id serial PRIMARY KEY,
    title  varchar(45) default null,
    season varchar(45) not null,
    email varchar(45) not null,
    foreign key (email) references admin(Email)
         on delete cascade 
         on update cascade
)